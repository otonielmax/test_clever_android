package com.otoniel.testotoandroid.domain

import com.otoniel.testotoandroid.data.network.RickAndMortyRepository
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import retrofit2.Response
import javax.inject.Inject

class GetCharacterByIdUseCase @Inject constructor(
    private val repository: RickAndMortyRepository
) {

    suspend operator fun invoke(id: Int) : Response<CharacterResponse> = repository.getCharacterById(id)
}