package com.otoniel.testotoandroid.domain

import com.otoniel.testotoandroid.data.network.RickAndMortyRepository
import com.otoniel.testotoandroid.data.network.response.BaseResponse
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import retrofit2.Response
import javax.inject.Inject

class GetCharacterUseCase @Inject constructor(
    private val repository: RickAndMortyRepository
) {

    suspend operator fun invoke(page: Int) : Response<BaseResponse<MutableList<CharacterResponse>>> = repository.getCharacters(page)
}