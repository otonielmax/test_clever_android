package com.otoniel.testotoandroid.data.network

import com.otoniel.testotoandroid.data.network.response.BaseResponse
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import retrofit2.Response
import javax.inject.Inject

class RickAndMortyRepository @Inject constructor(
    private val services: RickAndMortyServices
) {

    suspend fun getCharacters(page: Int): Response<BaseResponse<MutableList<CharacterResponse>>> {
        return services.getCharacters(page)
    }

    suspend fun getCharacterById(id: Int): Response<CharacterResponse> {
        return services.getCharacterById(id)
    }
}