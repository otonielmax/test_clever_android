package com.otoniel.testotoandroid.data.network

import com.otoniel.testotoandroid.data.network.response.BaseResponse
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class RickAndMortyServices @Inject constructor(
    private val api: ApiClient
) {

    suspend fun getCharacters(page: Int): Response<BaseResponse<MutableList<CharacterResponse>>> {
        return withContext(Dispatchers.IO) {
            api.getCharacters(page)
        }
    }

    suspend fun getCharacterById(id: Int): Response<CharacterResponse> {
        return withContext(Dispatchers.IO) {
            api.getCharacterById(id)
        }
    }
}