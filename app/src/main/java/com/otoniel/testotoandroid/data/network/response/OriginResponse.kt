package com.otoniel.testotoandroid.data.network.response

data class OriginResponse(
    val name: String,
    val url: String
)