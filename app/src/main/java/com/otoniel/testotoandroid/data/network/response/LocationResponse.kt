package com.otoniel.testotoandroid.data.network.response

data class LocationResponse(
    val name: String,
    val url: String
)