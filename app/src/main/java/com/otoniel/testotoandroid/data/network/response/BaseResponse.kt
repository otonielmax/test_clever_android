package com.otoniel.testotoandroid.data.network.response

import java.util.*

data class BaseResponse<T>(
    val info: InfoResponse,
    val results: T
)