package com.otoniel.testotoandroid.data.network.response

data class CharacterResponse(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: OriginResponse,
    val location: LocationResponse,
    val image: String,
    val episode: Array<String>,
    val url: String,
    val created: String,
)