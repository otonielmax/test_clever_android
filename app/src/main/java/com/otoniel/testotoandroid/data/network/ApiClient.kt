package com.otoniel.testotoandroid.data.network

import com.otoniel.testotoandroid.data.network.response.BaseResponse
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiClient {

    @GET("character")
    suspend fun getCharacters(@Query("page") page: Int) : Response<BaseResponse<MutableList<CharacterResponse>>>

    @GET("character/{id}")
    suspend fun getCharacterById(@Path("id") id: Int) : Response<CharacterResponse>
}