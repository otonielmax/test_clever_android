package com.otoniel.testotoandroid.util

object Constant {

    public const val URL = "https://rickandmortyapi.com/api/"

    public const val EXTRA_CHARACTER_ID = "EXTRA_CHARACTER_ID"
}