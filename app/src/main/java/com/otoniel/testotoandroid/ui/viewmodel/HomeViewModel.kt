package com.otoniel.testotoandroid.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.otoniel.testotoandroid.data.network.response.BaseResponse
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import com.otoniel.testotoandroid.data.network.response.InfoResponse
import com.otoniel.testotoandroid.domain.GetCharacterByIdUseCase
import com.otoniel.testotoandroid.domain.GetCharacterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getCharacterUseCase: GetCharacterUseCase,
    private val getCharacterByIdUseCase: GetCharacterByIdUseCase
) : ViewModel() {

    private var pageCurrent = 0
    private var listCharacter: MutableList<CharacterResponse> = mutableListOf()
    public lateinit var infoResponse: InfoResponse
    val listCharacterLiveData = MutableLiveData<MutableList<CharacterResponse>>()
    private lateinit var characterSelected: CharacterResponse
    val characterSelectedLiveData = MutableLiveData<CharacterResponse>()

    fun getCharacters() {
        pageCurrent++
        if (listCharacter.isNullOrEmpty()) {
            listCharacter = mutableListOf()
        }
        viewModelScope.launch {
            val result = getCharacterUseCase(pageCurrent)

            if (result.isSuccessful) {
                infoResponse = result.body()!!.info
                listCharacter.addAll(result.body()!!.results)
                listCharacterLiveData.postValue(listCharacter)
            }
        }
    }

    fun getCharacterById(id: Int) {
        viewModelScope.launch {
            val result = getCharacterByIdUseCase(id)

            if (result.isSuccessful) {
                characterSelected = result.body()!!
                characterSelectedLiveData.postValue(characterSelected)
            }
        }
    }
}