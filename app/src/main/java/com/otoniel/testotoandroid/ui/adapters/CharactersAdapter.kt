package com.otoniel.testotoandroid.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.otoniel.testotoandroid.ui.view.DetailsActivity
import com.otoniel.testotoandroid.R
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import com.otoniel.testotoandroid.databinding.ItemCharacterBinding
import com.otoniel.testotoandroid.util.Constant.EXTRA_CHARACTER_ID

class CharactersAdapter(
    private val context: Context,
    private val list: MutableList<CharacterResponse>,
    private val listener: CharacterListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class ItemCharacterHolder(private val binding: ItemCharacterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CharacterResponse) {
            Glide.with(context)
                .load(item.image)
                .placeholder(R.drawable.image_not_found)
                .into(binding.image)
            binding.name.text = item.name
            binding.status.text = "${item.status} - ${item.species}"
            binding.gender.text = item.gender
            binding.location.text = item.location.name

            binding.content.setOnClickListener {
                val i = Intent(context, DetailsActivity::class.java)
                i.putExtra(EXTRA_CHARACTER_ID, item.id)
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(i)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemCharacterHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val h: ItemCharacterHolder = holder as ItemCharacterHolder
        h.bind(list[position])
        if (position == list.lastIndex && listener != null) {
            listener.onLoadMoreItems()
        }
    }

    override fun getItemCount(): Int = list.size

    fun updateData(newData: MutableList<CharacterResponse>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    interface CharacterListener {
        fun onLoadMoreItems()
    }
}