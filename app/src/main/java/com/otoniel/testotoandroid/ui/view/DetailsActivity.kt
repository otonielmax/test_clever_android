package com.otoniel.testotoandroid.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.otoniel.testotoandroid.R
import com.otoniel.testotoandroid.data.network.response.CharacterResponse
import com.otoniel.testotoandroid.databinding.ActivityDetailsBinding
import com.otoniel.testotoandroid.ui.viewmodel.HomeViewModel
import com.otoniel.testotoandroid.util.Constant.EXTRA_CHARACTER_ID
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding

    private val homeViewModel : HomeViewModel by viewModels()

    private var id: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (intent.hasExtra(EXTRA_CHARACTER_ID)) {
            id = intent.getIntExtra(EXTRA_CHARACTER_ID, -1)
        }

        suscribeViewModel()
        loadData()
    }

    fun loadData() {
        if (id != -1) {
            homeViewModel.getCharacterById(id)
        } else {
            Toast.makeText(applicationContext, "Personaje invalido", Toast.LENGTH_LONG).show()
        }
    }

    fun suscribeViewModel() {
        homeViewModel.characterSelectedLiveData.observe(this, Observer {
            binding.contentLoading.isVisible = false
            binding.contentDetails.isVisible = true
            updateCharacterView(it)
        })
    }

    fun updateCharacterView(item: CharacterResponse) {
        Glide.with(applicationContext)
            .load(item.image)
            .placeholder(R.drawable.image_not_found)
            .into(binding.image)

        binding.name.text = item.name
        binding.status.text = "${item.status} - ${item.species}"
        binding.gender.text = item.gender
        binding.location.text = item.location.name
    }
}