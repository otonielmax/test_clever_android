package com.otoniel.testotoandroid.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.otoniel.testotoandroid.databinding.ActivityMainBinding
import com.otoniel.testotoandroid.ui.adapters.CharactersAdapter
import com.otoniel.testotoandroid.ui.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), CharactersAdapter.CharacterListener {

    private lateinit var binding: ActivityMainBinding

    private val homeViewModel : HomeViewModel by viewModels()

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: CharactersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        suscribeViewModel()
        initView()
    }

    override fun onStart() {
        super.onStart()

        loadData()
    }

    fun loadData() {
        homeViewModel.getCharacters()
    }

    fun initView() {
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        adapter = CharactersAdapter(applicationContext, mutableListOf(), this)
        binding.recycler.adapter = adapter
        binding.recycler.layoutManager = linearLayoutManager
    }

    fun suscribeViewModel() {
        homeViewModel.listCharacterLiveData.observe(this, Observer {
            binding.containerLoading.isVisible = false
            binding.recycler.isVisible = true
            adapter.updateData(it)
        })
    }

    override fun onLoadMoreItems() {
        binding.recycler.isVisible = false
        binding.containerLoading.isVisible = true
        homeViewModel.getCharacters()
    }
}